import Foundation
import UIKit


protocol HomeRouterProtocol: AnyObject {
    
}

protocol HomePresenterProtocol: AnyObject {
	func numberOfRowsInSection() -> Int
	func cellForRowAt(indexPath: IndexPath, tableView: UITableView) -> UITableViewCell
	func getTableView(tableView: UITableView)
	func getBooksList()
	func getBookListSuccess(result: BookDataWrapper)
	func getBookListFailure()
}


protocol HomeInteractorProtocol: AnyObject {
    var presenter: HomePresenterProtocol?  { get set }
	
	func invokeBookService()
}


protocol HomeViewProtocol: AnyObject {
    var presenter: HomePresenterProtocol?  { get set }
	
	func reloadDataTableView()
	func showLoading()
	func hideLoading()
}
