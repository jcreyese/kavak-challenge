import UIKit
import Kingfisher


class HomePresenter: HomePresenterProtocol {

    
    weak var view: HomeViewProtocol?
    var interactor: HomeInteractorProtocol?
    var router: HomeRouterProtocol?
	
	var booksList: [Book] = []
	var booksTableView: UITableView = UITableView()
    
	func numberOfRowsInSection() -> Int {
		return booksList.count
	}
	
	func cellForRowAt(indexPath: IndexPath, tableView: UITableView) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "BookTableViewCell") as! BookTableViewCell
		let book = booksList[indexPath.row]
		cell.bookTitleLabel.text = book.title
		cell.bookAuthorLabel.text = book.author
		
		if let path = book.img {
			let urlString = "\(path)"
			cell.bookImage.kf.indicatorType = .activity
			cell.bookImage.kf.setImage(with: URL(string: urlString), placeholder: UIImage(named: "placeholder"), options: [.transition(.fade(0.5))])
		}
		
		cell.selectionStyle = .none
		
		return cell
	}
	
	func getTableView(tableView: UITableView) {
		self.booksTableView = tableView
	}
	
	
	// Service characters
	func getBooksList() {
		self.view?.showLoading()
		interactor?.invokeBookService()
	}

	func getBookListSuccess(result: BookDataWrapper) {
		if let books = result.results?.books {
			booksList = books
			view?.reloadDataTableView()
			self.view?.hideLoading()
		}
	}

	func getBookListFailure() {
		self.view?.hideLoading()
	}
	
}
