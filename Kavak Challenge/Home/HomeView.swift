import UIKit


class HomeView: UIViewController, HomeViewProtocol {
    
	
    var presenter: HomePresenterProtocol?
    
	@IBOutlet weak var tableView: UITableView!
	
	
	override func viewDidLoad() {
        super.viewDidLoad()
		
		self.title = "Books"
		registerCell(tableView: tableView)
		setUpTableView()
		self.presenter?.getTableView(tableView: tableView)
		self.presenter?.getBooksList()
    }
	
	func registerCell(tableView: UITableView) {
		let identifier = "BookTableViewCell"
		tableView.register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
	}
	
	func setUpTableView() {
		tableView.delegate = self
		tableView.dataSource = self
		tableView.estimatedRowHeight = 120.0
	}
	
	func reloadDataTableView() {
		tableView.reloadData()
	}
	
	func showLoading() {
		self.showSpinner()
	}
	
	func hideLoading() {
		self.hideSpinner()
	}
    
}


// MARK: - UITableViewDelegate & UITableViewDataSource
extension HomeView: UITableViewDelegate, UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.presenter?.numberOfRowsInSection() ?? 0
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		return presenter?.cellForRowAt(indexPath: indexPath, tableView: tableView) ?? UITableViewCell()
	}
}
