import UIKit


class HomeInteractor: HomeInteractorProtocol {
    
    
    weak var presenter: HomePresenterProtocol?
    
    
	func invokeBookService() {
		let service = APIRestService(urlServer: "https://raw.githubusercontent.com/ejgteja/files/main/books.json")
		service.getBooks()
		service.completionHandlerBook { [weak self] books, status, message in
			if status {
				guard let self = self else { return }
				guard let _books = books else { return }
				self.presenter?.getBookListSuccess(result: _books)
			} else {
				self?.presenter?.getBookListFailure()
			}
		}
	}
}
