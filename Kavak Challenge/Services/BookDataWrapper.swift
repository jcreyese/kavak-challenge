//
//  BookDataWrapper.swift
//  Kavak Challenge
//
//  Created by Reyes, Julio César on 29/08/2021.
//

import Foundation

struct BookDataWrapper: Codable {
	var results: Books?
}

struct Books: Codable {
	var books: [Book]?
}

struct Book: Codable {
	var title: String?
	var author: String?
	var description: String?
	var genre: String?
	var img: String?
}
