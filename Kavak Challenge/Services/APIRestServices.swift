import Foundation
import Alamofire


class APIRestService {
	
	
	fileprivate var urlServer = ""
	typealias booksCallBack = (_ book: BookDataWrapper?, _ status: Bool, _ message: String) -> Void
	var bookCallBack: booksCallBack?
	
	
	// Init
	init(urlServer: String) {
		self.urlServer = urlServer
	}
	
	
	// GetBooksService
	func getBooks() {
		let url = "\(self.urlServer)"
		AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil, interceptor: nil, requestModifier: nil).response { (responseData) in
			guard let data = responseData.data else {
				self.bookCallBack?(nil, false, "")
				return
			}
			do {
				let books = try JSONDecoder().decode(BookDataWrapper.self, from: data)
				self.bookCallBack?(books, true, "")
				debugPrint("\n👨‍💻 NETWORK RESPONSE FOR DEBUG -> \(books)")
			} catch {
				self.bookCallBack?(nil, false, error.localizedDescription)
				debugPrint("\n👨‍💻 NETWORK RESPONSE ERROR FOR DEBUG -> \(error.localizedDescription)")
			}
		}
	}
	
	func completionHandlerBook(callBack: @escaping booksCallBack) {
		self.bookCallBack = callBack
	}
	
}
